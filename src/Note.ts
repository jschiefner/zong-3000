import { calculateDuration } from './utils';
import Track from './Track.js';

const STOP_DURATION = 10;
const NOTES = ['c', 'cs', 'd', 'ds', 'e', 'f', 'fs', 'g', 'gs', 'a', 'as', 'b'];
const FIXED_NOTE: ['a', 4] = ['a', 4];
const FIXED_FREQUENCY = 440;
const EQUAL_TEMPERED_FACTOR = Math.pow(2, 1 / 12);

export default class Note {
  track: Track;
  note: NoteDefintion;
  firstOfPair: boolean;
  constructor(track: Track, note: NoteDefintion) {
    this.track = track;
    this.note = note;
    this.firstOfPair = false;
  }

  play() {
    this.track.oscillator.setFrequency(this.frequency);
    setTimeout(this.stop.bind(this), this.playLengthMS);
  }

  stop() {
    this.track.oscillator.setFrequency(0);
  }

  get frequency() {
    if (this.note[0] === 'r') {
      return 0;
    }

    return FIXED_FREQUENCY * Math.pow(EQUAL_TEMPERED_FACTOR, this.halfStepsFromFixedNote);
  }

  get halfStepsFromFixedNote() {
    if (this.note[0] === 'r') {
      return 0;
    }

    const halfSteps = NOTES.indexOf(this.note[0][0]) - NOTES.indexOf(FIXED_NOTE[0]);
    const octaveHalfSteps = (this.note[0][1] - FIXED_NOTE[1]) * 12;
    return halfSteps + octaveHalfSteps;
  }

  get duration() {
    return calculateDuration(
      this.note[1],
      this.track.song.tempo,
      this.track.song.resolution,
      this.track.song.timing,
      this.firstOfPair,
    );
  }

  get playLengthMS() {
    return this.duration - STOP_DURATION;
  }
}
