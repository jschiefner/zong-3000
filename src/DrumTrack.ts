import DrumKick from './DrumKick';
import Noise from './Noise';
import Song from './Song';

const KICK_LENGTHS = {
  'h': 10,
  's': 100,
}

export default class DrumTrack {
  song: Song;
  noise: Noise;
  kicksDefinition: DrumKickDefinition[];
  constructor(song: Song, kicks: DrumTrackDefinition) {
    this.song = song;
    this.noise = new Noise(song.context);
    this.kicksDefinition = kicks;
  }

  get kicks() {
    return this.kicksDefinition.map(kick => kick ? new DrumKick(this, KICK_LENGTHS[kick]) : null);
  }

  playKick(index: number) {
    const kick = this.kicks[index % this.kicks.length];
    if (kick) {
      kick.play();
    }
  }
}
