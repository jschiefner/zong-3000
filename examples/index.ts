import { Song } from '../src/index';
import songDefinition from './let-it-snow/song';

const AudioContext = ((<any>window).AudioContext || (<any>window).webkitAudioContext);
const context: AudioContext = new AudioContext();
const song = new Song(songDefinition, context);

const playPause = document.querySelector('[data-playpause]');
if (playPause) {
  playPause.addEventListener('click', function () {
    if (song.isPlaying()) {
      song.pause();
    } else {
      song.play();
    }
  });
}

const stop = document.querySelector('[data-stop]');
if (stop) {
  stop.addEventListener('click', function() {
    song.stop();
  });
}

const tempo = (<HTMLInputElement>document.querySelector('input[data-tempo]'));
tempo.value = song.tempo.toString();
if (tempo) {
  tempo.addEventListener('change', function () {
    song.tempo = Number.parseInt(tempo.value);
  })
}

const timing = (<HTMLSelectElement>document.querySelector('select[data-timing]'));
timing.value = song.timing;
if (timing) {
  timing.addEventListener('change', function () {
    song.timing = (<Timing>timing.value);
  })
}
