# ZONG 3000 – 8bit music for the cyber future

## Examples

See [https://pmk1c.gitlab.io/zong-3000/](https://pmk1c.gitlab.io/zong-3000/) for examples.

## Instructions

Install necessary npm packages:

```
npm install
```

Start the Webpack dev server to serve the example page:

```
npm start
```
